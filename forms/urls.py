from django.urls import path
from . import views

app_name = 'forms'

urlpatterns = [
    path('', views.index, name='forms'),
    path('submit/', views.submit, name="submit"),
	path('delete/<int:friendsModel_id>/', views.deleteItem, name="delete")
]