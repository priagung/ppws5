# Generated by Django 3.0.3 on 2020-02-26 12:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('forms', '0006_auto_20200226_1908'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='friendsmodel',
            name='classYear',
        ),
        migrations.AddField(
            model_name='friendsmodel',
            name='ClassYear',
            field=models.CharField(default=2018, max_length=5),
        ),
        migrations.DeleteModel(
            name='YearClass',
        ),
    ]
