from django.shortcuts import render, redirect
from .models import friendsModel
from django.views.decorators.csrf import csrf_exempt

# Create your views here.
def index(request):
	friends = friendsModel.objects.all()
	content = {
		"friendList" : friends
	}
	return render(request, 'forms.html', content)

@csrf_exempt
def submit(request):
	friends_name = request.POST.get('nama')
	friends_hobby = request.POST.get('hobby')
	friends_favfood = request.POST.get('favfood')
	friends_classYear = request.POST.get('classYear')
	new_friend = friendsModel(name=friends_name, hobby=friends_hobby, favFood=friends_favfood, ClassYear=friends_classYear)
	new_friend.save()
	return redirect('forms:forms')
	
	
def deleteItem(request, friendsModel_id):
    item = friendsModel.objects.get(id=friendsModel_id)
    item.delete()
    return redirect('forms:forms')
